﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Datsuni
{
    class DriverAI
    {
        // 1.5 is quite good in keimola 1.57
        private const double _tractionPoint = 0.131;
        private const double _curveDecreasingFactor = 0.98;
        private const double _throttleToStartLearning = 0.4;
        private double _sideTickForceToLoseTraction = 0.243; // works in keimola: 0.243, works in germany: 0.255625, works in usa: 0.259
        
        private double _centrifugalForceToLoseTraction = 0.45;//0.4858;0.495
        private double _centrifugalForceChangeFactor = 0.98;
        private double _degreesPerForceChange = 3.0;
        
        private double _curveAccelerationRatio = 0.3; // How much of the curve end will be accelerating
        
        private double _maximumCarAngle = 59.0;
        private double _breakOnAngle = 50.0;
        private double _carAngleToIncreaseForce = 55.0;
        private double _measuredCurveCarAngle = 0.0;
        private double _measuredLapCarAngle = 0.0;
        
        private bool _inStraight = true;

        private List<CarPosition> _carPositions = null;
        private int _curveCount = 0;
        private double _initialThrottle = 1.0;
        public DriverAI()
        {
            Id = new CarId();
            Throttle = _initialThrottle;
            _previousForcewhenIncreasing = _centrifugalForceToLoseTraction;
        }

        public enum DrivingStatus
        {
            LearningAcceleration,
            LearningTraction,
            LearningDecceleration,
            Qualification,
            Race
        }

        public CarId Id { get; set; }
        private Race _race;
        public Race Race
        {
            get
            {
                return _race;
            }
            private set
            {
                if (_race == null)
                {
                    _race = value;
                }
            }
        }
        public double Throttle { get; private set; }
        public string SwitchLane { get; set; }
        private int _pendingLaneIndex = -1;
        public Turbo Turbo { get; set; }
        public bool TurboInUse { get; set; }
        public bool Bumping { get; private set; }
        public bool TurboStarted {get; set;}
        public bool TakingOver { get; private set; }

        private bool _useTurbo = false;
        public bool UseTurbo 
        { 
            get
            {
                return _useTurbo /*&& Race.Track.Pieces[_currentPosition.Position.PieceIndex].IsStraight*/;
            }
            set
            {
                _useTurbo = value;
            }
        }

        public Car Car { get; private set; }
        public Dictionary<CarId, Lap> Laps { get; private set; }

        private DrivingStatus _status = DrivingStatus.LearningAcceleration;
        private CarPosition _currentPosition;
        private CarPosition _previousPosition;
        private double _currentVelocity = 0.0;
        private double _previousVelocity = 0.0;
        private double _decceleration = 0.0;
        private double _maxVelocity = 0.0;

        double decceleration1 = 0.0, decceleration2 = 0.0, decceleration3 = 0.0;
        double acceleration1 = 0.0, acceleration2 = 0.0;
        int accelerationLearningTicks = 50;

        private int _nextLaneIndex = -1;
        private int _finishLaneIndex = 0;

        private List<Straight> _longestStraightIndices = new List<Straight>();

        private double _tractionLearningVelocity;

        private double _previousForcewhenIncreasing = 0.0;
        private double _forceWhenCrashing = 0.0;
        private double MaxCentrifugalForce 
        {
            get
            {
                return _centrifugalForceToLoseTraction;
            }
            set
            {
                _centrifugalForceToLoseTraction = value;
                
                PopulateMaxVelocities();
                foreach (TrackPiece clearPiece in Race.Track.Pieces)
                {
                    if (clearPiece.IsSwitch)
                    {
                        clearPiece.TicksToNextSwitchByLane = new List<double>();
                    }
                }
                CalculateOptimalLanes();
            }
        }
        
        public void Init(Race race)
        {
            Race = race;

            MaxCentrifugalForce = _centrifugalForceToLoseTraction;
            CalculateLongestStraight();
        }

        public void Init(RaceSession session)
        {
            _currentPosition = null;
            _previousPosition = null;
            _currentVelocity = 0.0;
            _previousVelocity = 0.0;
            _nextLaneIndex = -1;
            _pendingLaneIndex = -1;

            Throttle = 1.0;
            SwitchLane = "";
            Race.Session = session;

            //TODO: it would be wise to check if it's actual race
            _status = DrivingStatus.Race;
        }

        public void Crashed()
        {
            TurboInUse = false;
            Turbo = null; // TODO: is turbo lost on crash?
            Bumping = false;
            TurboStarted = false;
            TakingOver = false;
            _nextLaneIndex = -1;
            _pendingLaneIndex = -1;
            SwitchLane = "";
            _currentVelocity = 0.0;
            _previousVelocity = 0.0;
            Throttle = 1.0;
            // TODO: figure out was this due to bump from a car!!!
            //if (_previousForcewhenIncreasing < MaxCentrifugalForce)
            //{
            //    MaxCentrifugalForce = _previousForcewhenIncreasing + ((MaxCentrifugalForce - _previousForcewhenIncreasing) / 2);
            //}
            //else

            

            if (!Car.GotBumpedInLap) // Don't decrease if we just got bumped
            {
                _forceWhenCrashing = MaxCentrifugalForce;
            }

          //  if (_status != DrivingStatus.Race)
            {
                MaxCentrifugalForce *= 0.9; // Try to learn again
            }
            //else
            //{
            //    MaxCentrifugalForce *= _centrifugalForceChangeFactor;
            //}
            Console.WriteLine("Decreasing CentrifugalForce to: " + MaxCentrifugalForce);
        }

        private double CountCurrentCentrifugalForce()
        {
            double force = 0.0;
            if (_currentVelocity != 0.0 && _currentPosition != null)
            {
                TrackPiece piece = Race.Track.Pieces[_currentPosition.Position.PieceIndex];
                if(!piece.IsStraight)
                {
                    double laneRadius = 0.0;
                    Lane lane = Race.Track.Lanes[_currentPosition.Position.Lane.EndLaneIndex];
                    if (piece.Angle < 0.0)
                    {
                        // Left curve
                        laneRadius = piece.Radius + lane.DistanceFromCenter;
                    }
                    else
                    {
                        // Right curve
                        laneRadius = piece.Radius - lane.DistanceFromCenter;
                    }
                    force = _currentVelocity * _currentVelocity / laneRadius;
                }
            }
            return force;
        }

        public void LapFinished(Lap lap)
        { 
            if(lap.Id.Equals(Car.Id))
            {
                // Our car
                // Don't increase force if we drove behind another car
                if (_measuredLapCarAngle < _carAngleToIncreaseForce && !Car.BumpedInLap)
                {
                    //if (_measuredLapCarAngle < _carAngleToIncreaseForce - 10)
                    //{
                    //    double diff = _carAngleToIncreaseForce - _measuredLapCarAngle;
                    //    if(diff > 0)
                    //    {
                    //        _previousForcewhenIncreasing = MaxCentrifugalForce;
                    //        while(diff >= 0.0)
                    //        {
                    //            MaxCentrifugalForce /= _centrifugalForceChangeFactor;
                    //            diff -= _degreesPerForceChange; // TODO: correct value
                    //        }
                    //    }
                    //}
                    //else
                    //{
                    //    // Increase force as there is no critical angle
                    double newForce = MaxCentrifugalForce / _centrifugalForceChangeFactor;
                    if (newForce < _forceWhenCrashing || _forceWhenCrashing == 0.0)
                    {
                        MaxCentrifugalForce = newForce;
                        Console.WriteLine("Increasing CentrifugalForce to: " + MaxCentrifugalForce);
                    }
                    //}
                }
                
                Console.WriteLine("Maximum car angle was " + _measuredLapCarAngle);
                
                _measuredLapCarAngle = 0.0;
                Car.GotBumpedInLap = false;
                Car.BumpedInLap = false;
            }
            Console.WriteLine("Car: " + lap.Id.Name + " " + lap.Id.Color + ", Lap: " + lap.LapTime.Lap + ", Time: " + lap.LapTime.Millis / 1000.0);
            Console.WriteLine("Rank: " + lap.Ranking.Overall);
        }

        public void UpdateCarPositions(List<CarPosition> positions)
        {
            if (_carPositions != null)
            {
                foreach (CarPosition p in positions)
                {
                    var prev = (from c in _carPositions where c.Id.Equals(p.Id) select c).FirstOrDefault();
                    if (prev != null)
                    {
                        p.Velocity = CalculateDeltaPosition(prev.Position, p.Position);
                    }
                    else
                    {
                        p.Velocity = 0.0;
                    }
                }
            }

            _carPositions = positions.OrderBy(p => p.Position.PieceIndex).ToList<CarPosition>();
            

            foreach (CarPosition p in _carPositions)
            {
                if (p.Id.Equals(Id)) // Our car
                {
                    if (Car == null)
                    {
                        foreach (Car c in Race.Cars)
                        {
                            if (c.Id.Equals(Id))
                            {
                                Car = c;
                                break;
                            }
                        }
                    }

                    _previousPosition = _currentPosition;
                    _currentPosition = p;

                    if (_previousPosition != null && _currentPosition != null)
                    {
                        _previousVelocity = _currentVelocity;
                        _currentVelocity = CalculateDeltaPosition(_previousPosition.Position, _currentPosition.Position);

                        //Console.WriteLine("_currentVelocity: " + _currentVelocity);
                        //Console.WriteLine("current acceleration: " + (_currentVelocity - _previousVelocity));
                    }

                    //Console.WriteLine("Force: " + CountCurrentCentrifugalForce() + ", car angle: " + _currentPosition.Angle);
                    if (_status == DrivingStatus.LearningAcceleration)
                    {
                        if (_currentVelocity > 0.0)
                        {
                            accelerationLearningTicks--;
                            if (acceleration1 == 0.0)
                            {
                                acceleration1 = _currentVelocity - _previousVelocity;
                                Console.WriteLine("acceleration1: " + acceleration1);
                            }
                            else if (acceleration2 == 0.0)
                            {
                                acceleration2 = _currentVelocity - _previousVelocity;
                                //Console.WriteLine("acceleration2: " + acceleration2);
                                double accelerationDelta = acceleration2 / acceleration1;
                                _maxVelocity = _currentVelocity;
                                //Console.WriteLine("accelerationDelta: " + accelerationDelta);
                                while (acceleration2 > 0.00000001)
                                {
                                    acceleration2 *= accelerationDelta;
                                    // accelerationDelta = accelerationDelta * accelerationDelta;
                                    _maxVelocity += acceleration2;
                                }
                                Console.WriteLine("maxVelocity: " + _maxVelocity + ", acceleration2: " + acceleration2);
                                _status = DrivingStatus.LearningDecceleration;
                                //Console.WriteLine("Acceleration: " + acceleration1 + ", :" + acceleration2 + ", :" + acceleration3);
                                //Console.WriteLine("Acceleration change 1: " + acceleration2 / acceleration1 + ", 2: " + acceleration3 / acceleration2);
                            }
                        }
                    }
                    else if (_status == DrivingStatus.LearningTraction)
                    {

                        // Switch to inner lane to find out the traction as fast as possible
                        // TODO: is rightmost lane always the inner?
                        //if (_currentPosition.Position.Lane.EndLaneIndex < Race.Track.Lanes.Count - 1 &&
                        //    _nextLaneIndex != _currentPosition.Position.Lane.EndLaneIndex + 1)
                        //{
                        //    SwitchLane = "Right";
                        //    _nextLaneIndex = _currentPosition.Position.Lane.EndLaneIndex + 1;
                        //}

                        TrackPiece piece = Race.Track.Pieces[_currentPosition.Position.PieceIndex];
                        if (!piece.IsStraight)
                        {
                            _tractionLearningVelocity = _currentVelocity;

                            //Console.WriteLine("Angle is " + Math.Abs(_currentPosition.Angle) + ", relation to car length: " + (car.Dimensions.Length / piece.Radius));
                            double tractionPoint = Math.Abs(_currentPosition.Angle) * (Car.Dimensions.Length / piece.Radius);// / (_currentVelocity * _currentVelocity); // ennen ei kerrottu toiseen potenssiin

                            double centrifugal = _currentVelocity * _currentVelocity / piece.Radius;
                            if (centrifugal > _centrifugalForceToLoseTraction - 1)//_tractionPoint)
                            //  if (Math.Abs(_currentPosition.Angle) > 52.0D)
                            {
                                //if (_previousPosition.Angle == 0.0)
                                {
                                    // Car starts to lose traction! count the force
                                    if (_currentVelocity != 0.0)
                                    {
                                        _sideTickForceToLoseTraction = CountSideTickForceToLoseTraction(_currentVelocity);
                                        PopulateMaxVelocities();
                                        foreach (TrackPiece clearPiece in Race.Track.Pieces)
                                        {
                                            if (clearPiece.IsSwitch)
                                            {
                                                clearPiece.TicksToNextSwitchByLane = new List<double>();
                                            }
                                        }
                                        CalculateOptimalLanes();
                                        Console.WriteLine("Traction learning done, max side force: " + _sideTickForceToLoseTraction);
                                        if (_decceleration == 0.0)
                                        {
                                            _status = DrivingStatus.LearningDecceleration;
                                        }
                                        else
                                        {
                                            _status = DrivingStatus.Qualification;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                // On curves, lets acccelerate to find out the traction
                                _initialThrottle *= 1.001;
                                _initialThrottle = Math.Min(_initialThrottle, 1.0);
                                Throttle = _initialThrottle;
                            }
                        }
                        else
                        {
                            _initialThrottle *= 1.001;
                            _initialThrottle = Math.Min(_initialThrottle, 1.0);
                            Throttle = _initialThrottle;
                            // When learning traction, don't increase speed on straights
                            //if (_tractionLearningVelocity > 0.0)
                            //{
                            //    // Use factor just to make sure we don't go too fast
                            //    if (_currentVelocity >= _tractionLearningVelocity * 0.9)
                            //    {
                            //        Throttle = 0.0;
                            //    }
                            //    else
                            //    {
                            //        Throttle = 1.0;
                            //    }
                            //}
                        }
                    }
                    else if (_status == DrivingStatus.LearningDecceleration)
                    {
                        if (_currentVelocity > 0.0 && _previousVelocity != 0.0)
                        {
                            Throttle = 0.0;
                            double d = _currentVelocity / _previousVelocity;
                            Console.WriteLine("Decceleration change: " + d);
                            if (d < 1.0)
                            {
                                if (decceleration1 == 0.0)
                                {
                                    decceleration1 = d;
                                }
                                else if (decceleration2 == 0.0)
                                {
                                    decceleration2 = d;
                                }
                                else if (decceleration3 == 0.0)
                                {
                                    decceleration3 = d;
                                    _decceleration = (decceleration1 + decceleration2 + decceleration3) / 3;
                                    Console.WriteLine("Calculated decceleration: " + _decceleration);
                                    _status = DrivingStatus.Qualification;
                                }
                            }
                        }
                    }
                    else if (_status == DrivingStatus.Qualification || _status == DrivingStatus.Race)
                    {
                        if (_previousPosition != null && _currentPosition != null)
                        {
                            int pieceIndex = _currentPosition.Position.PieceIndex;
                            if (pieceIndex >= Race.Track.Pieces.Count)
                            {
                                pieceIndex = 0;
                            }
                            TrackPiece piece = Race.Track.Pieces[pieceIndex];

                            int previousIndex = pieceIndex - 1;
                            if (previousIndex < 0)
                            {
                                previousIndex = Race.Track.Pieces.Count - 1;
                            }
                            TrackPiece previousPiece = Race.Track.Pieces[previousIndex];

                            if (!piece.IsStraight)
                            {
                                Bumping = false;
                            }
                            else
                            {
                                // reset the started turbo
                                TurboStarted = false;
                            }

                            if(_measuredLapCarAngle < Math.Abs(_currentPosition.Angle))
                            {
                                // Measure the highest car angle of lap
                                _measuredLapCarAngle = Math.Abs(_currentPosition.Angle);
                            }

                            if (previousPiece.IsSwitch)
                            {
                                TakingOver = false;
                            }
                            
                            // TODO: observe if we are hit, don't track the car angle if so
                            if(_inStraight)
                            {
                                if(!piece.IsStraight)
                                {
                                    //curve entered
                                    _curveCount++;
                                    _measuredCurveCarAngle = Math.Abs(_currentPosition.Angle);
                                    _inStraight = false;
                                }
                                else
                                {
                                    // still in straight
                                    _measuredCurveCarAngle = 0.0;
                                }
                            }
                            else
                            {
                                if(!piece.IsStraight)
                                {
                                    // still in curve
                                    if(_measuredCurveCarAngle < Math.Abs(_currentPosition.Angle))
                                    {
                                        _measuredCurveCarAngle = Math.Abs(_currentPosition.Angle);
                                    } 
                                }
                                else
                                {
                                    // straight entered
                                    if (_measuredCurveCarAngle < Math.Abs(_currentPosition.Angle))
                                    {
                                        _measuredCurveCarAngle = Math.Abs(_currentPosition.Angle);
                                    }

                                    // Car angle was critical on the curve, decrease force
                                    if (_measuredCurveCarAngle > _maximumCarAngle)
                                    {
                                        MaxCentrifugalForce *= _centrifugalForceChangeFactor;
                                        Console.WriteLine("Decreasing CentrifugalForce to: " + _centrifugalForceToLoseTraction);
                                    }

                                    //if (_measuredLapCarAngle != 0.0 && 
                                    //    _measuredLapCarAngle < _maximumCarAngle - 14.0 &&
                                    //    _currentVelocity > 5.0 && _curveCount > 10 && _curveCount % 3 == 0)
                                    //{
                                    //    // We are way behind from maximum, try to increase force, but carefully 
                                    //    MaxCentrifugalForce /= _centrifugalForceChangeFactor;
                                    //    Console.WriteLine("Increasing CentrifugalForce to: " + MaxCentrifugalForce);
                                    //}
                                    
                                    _inStraight = true;
                                }
                            }

                            //else if (piece.IsStraight && _reduceCentrifugalForce)
                            //{
                             //   // Reduce once every straight if there was too much angle
                             //   _reduceCentrifugalForce = false;
                              //  _centrifugalForceToLoseTraction *= 0.98;
                               // Console.WriteLine("_centrifugalForceToLoseTraction is now: " + _centrifugalForceToLoseTraction);
                            //}

                            if (!piece.IsStraight)
                            {
                                if (Math.Abs(_currentPosition.Angle) > _breakOnAngle)
                                {
                                    // Save us quick from crashing!
                                    Console.WriteLine("Car angle is critical: " + _currentPosition.Angle);
                                    Throttle = 0.0;
                                    return;
                                }
                            }

                            DoRacingLogic();
                            

                            int nextIndex = pieceIndex + 1;
                            if (nextIndex >= Race.Track.Pieces.Count)
                            {
                                nextIndex = 0;
                            }
                            TrackPiece nextPiece = Race.Track.Pieces[nextIndex];

                            // TODO: should it be done a bit earlier as sometimes the switch is not done
                            if (nextPiece.IsSwitch && _pendingLaneIndex >= 0)
                            {
                                if (_pendingLaneIndex < _currentPosition.Position.Lane.EndLaneIndex)
                                {
                                    SwitchLane = "Left";
                                    _pendingLaneIndex = -1;
                                }
                                else if (_pendingLaneIndex > _currentPosition.Position.Lane.EndLaneIndex)
                                {
                                    SwitchLane = "Right";
                                    _pendingLaneIndex = -1;
                                }
                                else
                                {
                                    // Server is out of sync, reset to avoid complete switching stall!
                                    _pendingLaneIndex = -1;
                                }
                            }

                            bool longStraight = false;
                            int next = _currentPosition.Position.PieceIndex + 1;
                            if(next >= Race.Track.Pieces.Count)
                            {
                                next = 0;
                            }
                            foreach (Straight s in _longestStraightIndices)
                            {
                                if (s.Index == next)
                                {
                                    longStraight = true;
                                }
                            }
                            //if (Turbo != null && !Turbo.InUse && _currentPosition.Position.Lap != _previousPosition.Position.Lap)
                            if (Turbo != null &&
                                !TurboInUse &&
                                longStraight)
                            {
                                UseTurbo = true;
                            }

                            double optimal = OptimalVelocity();

                            // When running first round of qualification, try to clean your way by waiting a bit
                            // TODO: this really needs to be improved, take other car positions into account and no hard coding of distance in pieces
                            //if (_status == DrivingStatus.Qualification &&
                            //   _currentPosition.Position.Lap == -1 &&
                            //   (_currentPosition.Position.PieceIndex < _finishLaneIndex) &&
                            //   _currentPosition.Position.PieceIndex > _finishLaneIndex - 10)
                            //{
                            ////    Console.WriteLine("Slowing down to get clean lane on lap..." + _currentPosition.Position.Lap);
                            //    optimal = Math.Min(optimal, 5.0);
                            //}

                            //Console.WriteLine("Current velocity: " + _currentVelocity + ", optimal target: " + optimal);
                            if (_currentVelocity >= (optimal /*+ 0.3*/))
                            {
                                Throttle = 0.0;
                            }
                            else
                            {
                                Throttle = 1.0;
                            }
                        }
                    }
                }
            }
        }

        private void PopulateMaxVelocities()
        {
            foreach (TrackPiece piece in Race.Track.Pieces)
            {
                if (!piece.IsStraight)
                {
                    piece.MaxVelocitiesByLane.Clear();
                    foreach (Lane lane in Race.Track.Lanes)
                    {
                        piece.MaxVelocitiesByLane.Add(CountMaxVelocityPerTick(lane, piece));
                    }
                }
            }
        }

        private double CountSideTickForceToLoseTraction(double velocity)
        {
            TrackPiece piece = Race.Track.Pieces[_currentPosition.Position.PieceIndex];
            Lane lane = Race.Track.Lanes[_currentPosition.Position.Lane.EndLaneIndex];
            double laneRadius = 0.0;
            if (piece.Angle < 0.0)
            {
                // Left curve
                laneRadius = piece.Radius + lane.DistanceFromCenter;
            }
            else
            {
                // Right curve
                laneRadius = piece.Radius - lane.DistanceFromCenter;
            }
            return laneRadius - (Math.Cos(velocity / laneRadius) * laneRadius);
        }

        private double CalculateDeltaPosition(TrackPiecePosition oldPos, TrackPiecePosition newPos)
        {
            if (oldPos.PieceIndex == newPos.PieceIndex)
            {
                return newPos.InPieceDistance - oldPos.InPieceDistance;
            }
            else
            {
                TrackPiece oldPiece = Race.Track.Pieces[oldPos.PieceIndex];
                if (Race.Track.Pieces[oldPos.PieceIndex].IsStraight)
                {
                    return newPos.InPieceDistance + (oldPiece.Length - oldPos.InPieceDistance);
                }
                else
                {
                    double curveLength = CurveLength(oldPiece, Race.Track.Lanes[oldPos.Lane.EndLaneIndex]);
                    return newPos.InPieceDistance + (curveLength - oldPos.InPieceDistance);
                }
            }
        }

        private double CountMaxVelocityPerTick(Lane lane, TrackPiece piece)
        {
            if (!piece.IsStraight)
            {
                double laneRadius = 0.0;
                if (piece.Angle < 0.0)
                {
                    // Left curve
                    laneRadius = piece.Radius + lane.DistanceFromCenter;
                }
                else
                {
                    // Right curve
                    laneRadius = piece.Radius - lane.DistanceFromCenter;
                }

                //centrifugal force
                double velocity = Math.Sqrt(_centrifugalForceToLoseTraction * laneRadius);
                return velocity;

                // OLD CALCULATION
                //double catethusX = laneRadius - _sideTickForceToLoseTraction;
                //double hypotenuse = laneRadius;

                //double catethusY = Math.Sqrt(-((catethusX * catethusX) - (hypotenuse * hypotenuse)));

                //// maxCurveLength is the length that can be driven within one tick without losing traction
                //double maxCurveLength = Math.Asin(catethusY / hypotenuse) * laneRadius;

                //return maxCurveLength;
            }
            else
            {
                return 0.0;
            }
        }

        private double OptimalVelocity()
        {
            if(TurboStarted)
            {
                if (Race.Track.Pieces[_currentPosition.Position.PieceIndex].IsStraight)
                {
                    return double.MaxValue;
                }
            }
            
            double straightLength = 0.0;
            int i = _currentPosition.Position.PieceIndex;
            while (Race.Track.Pieces[i].IsStraight)
            {
                straightLength += Race.Track.Pieces[i].Length;
                if (_currentPosition.Position.PieceIndex == i)
                {
                    straightLength -= _currentPosition.Position.InPieceDistance;
                }
                i++;
                if (i >= Race.Track.Pieces.Count)
                {
                    i = 0;
                }
            }

            //Count the speed from which we can break before the curve
            double velocity = Race.Track.Pieces[i].MaxVelocitiesByLane[_currentPosition.Position.Lane.EndLaneIndex];

            // We are in curve, use decreased optimal until the half of the curve (of the whole curve with same angle, not remaining)
            int curveEnd = i;
            double angle = Race.Track.Pieces[i].Angle;
            while (!Race.Track.Pieces[curveEnd].IsStraight && Race.Track.Pieces[curveEnd].Angle == angle)
            {
                curveEnd++;
                if (curveEnd >= Race.Track.Pieces.Count)
                {
                    curveEnd = 0;
                }
            }
            curveEnd--;

            i = curveEnd;
            double curveLength = 0.0;
            double upcomingCurveLength = 0.0;
            int laneIndex = _currentPosition.Position.Lane.EndLaneIndex;
            if (_nextLaneIndex >= 0 && _nextLaneIndex < Race.Track.Lanes.Count)
            {
                laneIndex = _nextLaneIndex;
            }
            Lane lane = Race.Track.Lanes[laneIndex];
            while (!Race.Track.Pieces[i].IsStraight && Race.Track.Pieces[i].Angle == angle)
            {
                curveLength += CurveLength(Race.Track.Pieces[i], lane);
                if (i == _currentPosition.Position.PieceIndex)
                {
                    upcomingCurveLength = curveLength - _currentPosition.Position.InPieceDistance;
                }
                i--;
                if (i < 0)
                {
                    i = Race.Track.Pieces.Count - 1;
                }
            }
            //if (upcomingCurveLength == 0.0)
            //{
            //    // The whole curve is still to come
            //    upcomingCurveLength = curveLength;
            //}

            double nextStraightLength = 0.0;
            double nextCurveVelocity = 0.0;
            for (int nextCurveIndex = curveEnd + 1; ; nextCurveIndex++)
            {
                if (!Race.Track.Pieces[nextCurveIndex].IsStraight)
                {
                    // Decrease the velocity just to be sure we make it (and the subsequent curve too)
                    nextCurveVelocity = 0.96 * Race.Track.Pieces[nextCurveIndex].MaxVelocitiesByLane[_currentPosition.Position.Lane.EndLaneIndex];
                    break;
                }
                else
                {
                    nextStraightLength += Race.Track.Pieces[nextCurveIndex].Length;
                    if (nextCurveIndex == Race.Track.Pieces.Count - 1)
                    {
                        nextCurveIndex = 0;
                    }
                }
            }

            if (nextStraightLength + upcomingCurveLength > 0.0)
            {
                double length = nextStraightLength + upcomingCurveLength;
                while (length > 0.0)
                {
                    nextCurveVelocity = nextCurveVelocity / _decceleration;
                    length = length - nextCurveVelocity;
                }
            }
            //if (nextCurveVelocity < velocity)
            //    Console.WriteLine("max velocity: " + velocity + ", nextCurveVelocity: " + nextCurveVelocity);
            bool useNextCurveVelocity = velocity > nextCurveVelocity;
            velocity = Math.Min(nextCurveVelocity, velocity);

            // Convert curve velocity to current straight position velocity
            if (straightLength > 0.0)
            {
                while (straightLength > 0.0)
                {
                    velocity = velocity / _decceleration;
                    straightLength -= velocity;
                }
                //velocity = velocity / _decceleration;
            }
            else
            {
                // decrease the optimal on first half of the curve
                if ((upcomingCurveLength / curveLength) > _curveAccelerationRatio)
                {
                    //Console.WriteLine("..lowering the max velocity on curve");
                    if (nextStraightLength == 0.0)
                    {
                    }
                    velocity *= _curveDecreasingFactor;
                }
                else
                {
                    while (upcomingCurveLength > 0.0)
                    {
                        velocity = velocity / _decceleration;
                        upcomingCurveLength = upcomingCurveLength - velocity;
                    }
                    if (!useNextCurveVelocity)
                    {
                        velocity *= 1.3;
                    }
                }
             //   Console.WriteLine("OptimalVelocity: " + velocity + ", current: " + _currentVelocity);
            }

            //Console.WriteLine("OptimalVelocity: " + velocity + ", current: " + _currentVelocity);
            if (Bumping)
            {
                velocity *= 4.0;
            }
            else if (TakingOver /*&& Race.Track.Pieces[_currentPosition.Position.PieceIndex].IsStraight*/)
            {
                velocity *= 1.05; // TODO: VERIFY!!!
            }
            if (TurboStarted)
            {
                //still in curve...
                velocity *= 1.5;
            }
            return velocity;
        }

        private void CalculateOptimalLanes()
        {
            TrackPiece switchPiece = null;
            Lane lane = null;
            for (int i = 0; i < Race.Track.Lanes.Count; i++)
            {

                lane = Race.Track.Lanes[i];
                switchPiece = null;
                foreach (TrackPiece p in Race.Track.Pieces)
                {
                    if (p.IsSwitch)
                    {
                        switchPiece = p;
                        switchPiece.TicksToNextSwitchByLane.Add(0.0);
                    }
                    else if (!p.IsStraight)
                    {
                        double curveLength = CurveLength(p, lane);

                        if (switchPiece != null)
                        {
                            switchPiece.TicksToNextSwitchByLane[i] += curveLength / p.MaxVelocitiesByLane[i];
                        }
                    }
                }
            }
        }

        private double CurveLength(TrackPiece piece, Lane lane)
        {
            double laneRadius = 0.0;
            if (piece.Angle < 0.0)
            {
                // Left curve
                laneRadius = piece.Radius + lane.DistanceFromCenter;
            }
            else
            {
                // Right curve
                laneRadius = piece.Radius - lane.DistanceFromCenter;
            }

            return (2 * Math.PI * laneRadius) * (Math.Abs(piece.Angle) / 360);
        }

        private void DoRacingLogic()
        {
            TrackPiece currentPiece = Race.Track.Pieces[_currentPosition.Position.PieceIndex];
            int switchIndex = _currentPosition.Position.PieceIndex + 1;
            if(switchIndex >= Race.Track.Pieces.Count)
            {
                switchIndex = 0;
            }
            TrackPiece switchPiece = Race.Track.Pieces[switchIndex];

            while (!switchPiece.IsSwitch)
            {
                switchIndex++;
                if (switchIndex >= Race.Track.Pieces.Count)
                {
                    switchIndex = 0;
                }
                switchPiece = Race.Track.Pieces[switchIndex];
            }
            if (currentPiece.IsSwitch)
            {
                // reset the next switch, without this we might have a lock with switches when server doesn't switch the lane
                _nextLaneIndex = -1;
            }
            int afterSwitchIndex = switchIndex + 1;
            if (afterSwitchIndex >= Race.Track.Pieces.Count)
            {
                afterSwitchIndex = 0;
            }

            bool turboApplicableForTakingOver = //StraightOrCurveLength(afterSwitchIndex, 0) > Car.Dimensions.Length * 3.0 &&
                                                //Race.Track.Pieces[afterSwitchIndex].IsStraight &&
                                                Turbo != null &&
                                                !TurboInUse &&
                                                currentPiece.IsStraight &&
                                                StraightOrCurveLength(_currentPosition.Position.PieceIndex, 0) > (Car.Dimensions.Length * 7.0);

            // At first let's check a fixed length forward
            double lengthToCheck = Car.Dimensions.Length * 10.0;
            int piecesToCheck = 0;
            int lastIndexToCheck = 0;
            for(int x = _currentPosition.Position.PieceIndex; ;x++)
            {
                if(x >= Race.Track.Pieces.Count)
                {
                    x = 0;
                }
                TrackPiece p = Race.Track.Pieces[x];
                if(lengthToCheck > 0)
                {
                    piecesToCheck++;
                }
                else
                {
                    break;
                }
                double length = p.Length;
                if(!p.IsStraight)
                {
                    length = CurveLength(p, Race.Track.Lanes[0]); // Use 0 index, although not perfect
                }

                if(x == _currentPosition.Position.PieceIndex)
                {
                    lengthToCheck -= length - _currentPosition.Position.InPieceDistance;
                }
                else
                {
                    lengthToCheck -= length;
                }
            }
            lastIndexToCheck = _currentPosition.Position.PieceIndex + piecesToCheck - 1;
            if(lastIndexToCheck >= Race.Track.Pieces.Count)
            {
                lastIndexToCheck = lastIndexToCheck - Race.Track.Pieces.Count;
            }

            // Should we make a lane switch? loop for one lane from each side 
            int i = Math.Max(_currentPosition.Position.Lane.EndLaneIndex - 1, 0);
            int newIndex = i;
            double currentLaneTicks = switchPiece.TicksToNextSwitchByLane[_currentPosition.Position.Lane.EndLaneIndex];
            double smallestTicks = -1.0;
            List<Straight> carDistances = new List<Straight>();
            double newLaneCarDistance = double.MaxValue;
            for (; i < Math.Min(switchPiece.TicksToNextSwitchByLane.Count, _currentPosition.Position.Lane.EndLaneIndex + 1 + 1); i++)
            {
                double minDistance = double.MaxValue;
                //if (switchIndex == _currentPosition.Position.PieceIndex)
                {
                    //check if there are cars nearby in this lane
                    var cars = from c in _carPositions
                               where (c.Position.PieceIndex >= _currentPosition.Position.PieceIndex &&
                                      c.Position.PieceIndex < lastIndexToCheck &&
                                      c.Position.Lane.EndLaneIndex == i)
                               select c;
                    CarPosition nearestCarOnLane = null;
                    foreach (CarPosition p in cars)
                    {
                        // Don't mind this car if it is slightly behind or it has higher speed
                        if ((p.Position.PieceIndex == _currentPosition.Position.PieceIndex &&
                            p.Position.InPieceDistance < _currentPosition.Position.InPieceDistance) ||
                            (p.Velocity > _currentVelocity && !turboApplicableForTakingOver) ||
                            p.Id.Equals(Car.Id))
                        {
                            if (_currentPosition.Position.Lane.EndLaneIndex == p.Position.Lane.EndLaneIndex &&
                                (_currentPosition.Position.InPieceDistance - (Car.Dimensions.Length + 2.0)) < p.Position.InPieceDistance &&
                                _currentPosition.Position.PieceIndex == p.Position.PieceIndex &&
                                _currentPosition.Position.InPieceDistance > p.Position.InPieceDistance &&
                                p.Velocity > 0.0 &&
                                !p.Id.Equals(Car.Id))
                            {
                                // We got bumped
                                Car.GotBumpedInLap = true;
                                Console.WriteLine("Got bumped by car " + p.Id.Name + " " + p.Id.Color + ", pixels: " + (_currentPosition.Position.InPieceDistance - Car.Dimensions.Length - p.Position.InPieceDistance));
                            }
                            continue;
                        }

                        int pieceIndex = _currentPosition.Position.PieceIndex;
                        double length = 0.0;
                        while (pieceIndex != p.Position.PieceIndex + 1)
                        {
                            TrackPiece piece = Race.Track.Pieces[pieceIndex];
                            if (pieceIndex == p.Position.PieceIndex)
                            {
                                length += p.Position.InPieceDistance;
                            }
                            else if (!piece.IsStraight)
                            {
                                length += CurveLength(piece, Race.Track.Lanes[i]);
                            }
                            else
                            {
                                length += piece.Length;
                            }

                            pieceIndex++;
                            if(pieceIndex >= Race.Track.Pieces.Count)
                            {
                                pieceIndex = 0;
                            }
                        }

                        if (length < minDistance && length > 0.0)
                        {
                           // Console.WriteLine("Changing minDistance of lane " + i + " to " + length + " because car " + p.Id.Name + " has v" + p.Velocity + ". Our's is " + _currentVelocity);
                            minDistance = length;
                            nearestCarOnLane = p;
                        }
                    }

                    carDistances.Add(new Straight() { Index = i, Length = minDistance, Car = nearestCarOnLane });
                    if (nearestCarOnLane != null)
                    {
                        if (_currentPosition.Position.Lane.EndLaneIndex == nearestCarOnLane.Position.Lane.EndLaneIndex &&
                            minDistance < (Car.Dimensions.Length + 2.0))
                        {
                            // We probably or almost bumped into a car
                            Car.BumpedInLap = true;
                            Bumping = false;
                            Console.WriteLine("Bumped to car " + nearestCarOnLane.Id.Name + " " + nearestCarOnLane.Id.Color + ", pixels: " + (minDistance - Car.Dimensions.Length));
                        }
                    }
                    carDistances = carDistances.OrderByDescending(d => d.Length).ToList<Straight>();
                }

                if (switchPiece.TicksToNextSwitchByLane[i] < smallestTicks || smallestTicks < 0.0)
                {
                    smallestTicks = switchPiece.TicksToNextSwitchByLane[i];
                    newIndex = i;
                    newLaneCarDistance = minDistance;
                }
            }

            int optimalLane = newIndex;

            //Console.WriteLine("New optimal index is " + newIndex + ", current index: " + _currentPosition.Position.Lane.EndLaneIndex);
            double otherCarDistanceToCurve = StraightOrCurveLength(_currentPosition.Position.PieceIndex, newIndex) - newLaneCarDistance - _currentPosition.Position.InPieceDistance;
            //Console.WriteLine("StraightOrCurveLength(): " + StraightOrCurveLength(_currentPosition.Position.PieceIndex, newIndex) + ", newLaneCarDistance: " + newLaneCarDistance + ", InPieceDistance: " + _currentPosition.Position.InPieceDistance);
            bool tryToBump = false;
            bool useTurboOnBump = false;
          //  if (otherCarDistanceToCurve < newLaneCarDistance)
            {
                Bumping = false;
            }
            if (newLaneCarDistance < double.MaxValue)
            {
                tryToBump = //newLaneCarDistance < (Car.Dimensions.Length * 3.0) &&
                            // otherCarDistanceToCurve < (Car.Dimensions.Length * 10.0) &&
                            otherCarDistanceToCurve > newLaneCarDistance &&
                            currentPiece.IsStraight;
                if (Turbo != null && !TurboInUse)
                {
                    bool tryToBumpTurbo = //newLaneCarDistance > (Car.Dimensions.Length * 2.0) &&
                                //otherCarDistanceToCurve < (Car.Dimensions.Length * 6.0) &&
                                otherCarDistanceToCurve > newLaneCarDistance &&
                                currentPiece.IsStraight &&
                                newLaneCarDistance > (Car.Dimensions.Length * 2.5);
                    if(tryToBumpTurbo)
                    {
                        tryToBump = true;
                        useTurboOnBump = true;
                    }
                }
                //else
                //{

                  //  if (otherCarDistanceToCurve < (Car.Dimensions.Length * 10.0))
                  //  {
                    //    Console.WriteLine("otherCarDistanceToCurve: " + otherCarDistanceToCurve + ", newLaneCarDistance: " + newLaneCarDistance + ", Car.Dimensions.Length: " + Car.Dimensions.Length);
                   // }

                //}
            }

            bool newTakingOverDecision = false;
            if (carDistances.Count > 0)
            {
                double optimal = OptimalVelocity();
                // Select cars that could be taken over in 15 times of car's length distance
                Straight s = (from d in carDistances where (d.Index == newIndex && 
                                                           d.Length < double.MaxValue && d.Car != null &&
                                                           //(((d.Length / (((turboApplicableForTakingOver ? 3.0 : 1.0) * (_currentPosition.Velocity - d.Car.Velocity)))) * _currentPosition.Velocity) / Car.Dimensions.Length) < 15.0 &&
                                                           optimal > (d.Car.Velocity) &&
                                                           _currentPosition.Velocity > (d.Car.Velocity / 1.02) &&
                                                           (!tryToBump || _currentVelocity < (d.Car.Velocity * 1.2)) &&
                                                           (d.Car.Position.Lap <= _currentPosition.Position.Lap || !(useTurboOnBump && tryToBump)))
                                                           select d).FirstOrDefault();
                if (s != null && s.Length < double.MaxValue)
                {
                    TakingOver = true;
                    newTakingOverDecision = true;
                    Bumping = false;
                    
                    double distanceToTakeOver = ((s.Length / (((turboApplicableForTakingOver ? 3.0 : 1.0) * _currentPosition.Velocity) - s.Car.Velocity)) * _currentPosition.Velocity);
                    // There is a car in the optimal lane just car length ahead
                    var freeLanes = (from f in carDistances where 
                                    (f.Length == double.MaxValue ||
                                    (((f.Length / (((turboApplicableForTakingOver ? 3.0 : 1.0) * _currentPosition.Velocity) - f.Car.Velocity)) * _currentPosition.Velocity) / Car.Dimensions.Length) > 15.0) 
                                    select f).ToList<Straight>();

                    // Select free lane
                    smallestTicks = double.MaxValue;
                    if(freeLanes != null)
                    {
                        
                        for (i = 0; i < freeLanes.Count(); i++)
                        {
                            if (switchPiece.TicksToNextSwitchByLane[freeLanes[i].Index] < smallestTicks)
                            {
                                newIndex = freeLanes[i].Index;
                                smallestTicks = switchPiece.TicksToNextSwitchByLane[freeLanes[i].Index];
                          //      if (newIndex < _currentPosition.Position.Lane.EndLaneIndex && _nextLaneIndex != newIndex)
                                {
                                    if (turboApplicableForTakingOver)
                                    {
                                        UseTurbo = true;
                                    }
                                }
                            }
                        }
                        if (newIndex != optimalLane)
                        {
                            Console.WriteLine("Taking over by switching to non optimal clean lane " + newIndex);
                        }
                    }

                    if (smallestTicks == double.MaxValue)
                    {                      
                        newIndex = carDistances.Last().Index;
                        // there was no free lane, take second best according to distance to a car
                    //    if (newIndex < _currentPosition.Position.Lane.EndLaneIndex && _nextLaneIndex != newIndex)
                        {
                            if (newIndex != optimalLane)
                            {
                                Console.WriteLine("Taking over by switching to second most optimal lane " + newIndex);
                            }
                            if (turboApplicableForTakingOver)
                            {
                                UseTurbo = true;
                            }
                        }
                    }
                }
            }

            if (TakingOver && !newTakingOverDecision)
            {
                // We are taking over so don't override back to optimal lane
                return;
            }

            if (_nextLaneIndex != newIndex)
            {
                _pendingLaneIndex = newIndex;
                _nextLaneIndex = _pendingLaneIndex;
             //   Console.WriteLine("---pending " + _pendingLaneIndex);
            }

            if (tryToBump && !TakingOver)
            {
                Console.WriteLine("We see an opportunity to bump to a car to crash it");
                if (useTurboOnBump)
                {
                    UseTurbo = true;
                }
                Bumping = true;
            }
        }

        private double StraightOrCurveLength(int startIndex, int laneIndex)
        {
            int i = startIndex;
            double length = 0.0;
            if (Race.Track.Pieces[i].IsStraight)
            {
                while (Race.Track.Pieces[i].IsStraight)
                {
                    length += Race.Track.Pieces[i].Length;
                    i++;
                    if (i >= Race.Track.Pieces.Count)
                    {
                        i = 0;
                    }
                }
            }
            else 
            {
                while (!Race.Track.Pieces[i].IsStraight)
                {
                    length += CurveLength(Race.Track.Pieces[i], Race.Track.Lanes[laneIndex]);
                    i++;
                    if (i >= Race.Track.Pieces.Count)
                    {
                        i = 0;
                    }
                }
            }

            //Console.WriteLine("StraightOrCurveLength index " + startIndex + ", length: " + length);
            return length;
        }

        private void CalculateLongestStraight()
        {
            double length = 0;
            int startIndex = 0;
            double curveVelocity = 0.0;

            List<Straight> straights = new List<Straight>();

            int i = Race.Track.Pieces.Count - 1;
            // Count first the finishing lane
            int finishLanePieces = 0;
            for (; Race.Track.Pieces[i].IsStraight; i--)
            {
                finishLanePieces++;
                length += Race.Track.Pieces[i].Length;
            }

            curveVelocity = Race.Track.Pieces[i].MaxVelocitiesByLane[0]; // Just use simply lane 0

            if (length > 0)
            {
                startIndex = i + 1;
                _finishLaneIndex = startIndex;
            }

            for (i = 0; i < Race.Track.Pieces.Count - finishLanePieces; i++)
            {
                if (Race.Track.Pieces[i].IsStraight)
                {
                    length += Race.Track.Pieces[i].Length;
                }
                else
                {
                    length = length / curveVelocity; // Take previous curve velocity into account
                    if (straights.Count < 2)
                    {
                        straights.Add(new Straight() { Index = startIndex, Length = length });
                    }
                    else
                    {
                        foreach (Straight s in straights)
                        {
                            if (s.Length < length)
                            {
                                s.Length = length;
                                s.Index = startIndex;
                            }
                        }
                    }
                    curveVelocity = Race.Track.Pieces[i].MaxVelocitiesByLane[0]; // Just use simply lane 0
                    length = 0.0;
                    startIndex = i + 1;
                }
            }

            double small = Math.Min(straights[0].Length, straights[1].Length);
            double big = Math.Max(straights[0].Length, straights[1].Length);
            double relation = big / small;
            if (relation > 1)
            {
                //Use only one straight as there is so big difference
                if (straights[0].Length == small)
                {
                    straights.RemoveAt(0);
                }
                else 
                {
                    straights.RemoveAt(1);
                }
            }
            _longestStraightIndices = straights;
        }

    }

    public class Straight
    {
        public int Index;
        public double Length;
        public CarPosition Car;
    }
}
