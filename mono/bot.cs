using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.Collections.Generic;
using Datsuni;
using Newtonsoft.Json.Linq;

// https://helloworldopen.com/race/535d3912e4b0aeb0ab2cdab5
// https://helloworldopen.com/race/535d3e65e4b05715d95fc3e4

// Takeover USA: https://helloworldopen.com/race/535e80e4e4b0e941ca723c2e
// ramming france https://helloworldopen.com/race/535e899ce4b0c6663c3c45aa
// france victory https://helloworldopen.com/race/535ea826e4b0e941ca724f41
// https://helloworldopen.com/race/535eabede4b0e941ca7250b7

public class Bot {
	public static void Main(string[] args) {
	    string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];

		Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

		using(TcpClient client = new TcpClient(host, port)) {
			NetworkStream stream = client.GetStream();
			StreamReader reader = new StreamReader(stream);
			StreamWriter writer = new StreamWriter(stream);
			writer.AutoFlush = true;
            BotId id = new BotId();
            id.name = botName;
            id.key = botKey;
			new Bot(reader, writer, id);
		}
	}

	private StreamWriter writer;

	Bot(StreamReader reader, StreamWriter writer, BotId id) {
		this.writer = writer;
		string line;

        DriverAI driver = new DriverAI();

		
        send(new Join(id.name, id.key));
        //send(new CreateRace(id, "germany", "", 1));
        //send(new CreateRace(id, "usa", "", 1));
        //send(new CreateRace(id, "france", "", 1));
        //send(new JoinRace(id, "elaeintarha", "bar", 8));
        //send(new JoinRace(id, "suzuka", "foo", 4));

        //send(new JoinRace(id, "elaeintarha", "foo123", 6));
        //send(new JoinRace(id, "germany", "asdf", 8));

        bool crashed = false;

		while((line = reader.ReadLine()) != null) {
            DateTime time = DateTime.Now;
			try
			{
                MsgWrapperWithTick msg = JsonConvert.DeserializeObject<MsgWrapperWithTick>(line);
                JToken obj = null;
                CarId cid;
				switch(msg.msgType) {
					case "carPositions":
                        
						List<CarPosition> positions = JsonConvert.DeserializeObject<List<CarPosition>>(msg.data.ToString());
                        driver.UpdateCarPositions(positions);

                        if (!string.IsNullOrEmpty(driver.SwitchLane))
                        {
                            Console.WriteLine("Switching lane to " + driver.SwitchLane);
                            send(new SwitchLane(driver.SwitchLane, msg.gameTick));
                            driver.SwitchLane = ""; // Reset the switching
                        }
                        else if (driver.UseTurbo)
                        {
                            Console.WriteLine("Using TURBO!");
                            send(new UseTurbo("Datsuni hits pedal to the medal"));
                            driver.UseTurbo = false;
                            driver.TurboInUse = true;
                        }
                        else
                        {
                           // Console.WriteLine("Throttle " + driver.Throttle);
                            send(new Throttle(driver.Throttle, msg.gameTick));
                        }
						break;
                    case "lapFinished":
                        Lap lap = JsonConvert.DeserializeObject<Lap>(msg.data.ToString());
                        driver.LapFinished(lap);
                        send(new Throttle(driver.Throttle, msg.gameTick));
                        break;
					case "join":
						Console.WriteLine("Joined");
						send(new Ping());
						break;
                    case "yourCar":
                        obj = JToken.FromObject(msg.data);
                        driver.Id.Name = obj["name"].ToString();
                        driver.Id.Color = obj["color"].ToString();
                        Console.WriteLine("Datsuni is colored " + driver.Id.Color);
                        send(new Ping());
                        break;
                    case "turboAvailable":
                        Turbo turbo = JsonConvert.DeserializeObject<Turbo>(msg.data.ToString());
                        if (!crashed)
                        {
                            driver.Turbo = turbo;
                        }
                        Console.WriteLine("Turbo available: " + msg.data);

                        send(new Throttle(driver.Throttle, msg.gameTick));
                        break;
                    case "turboStart":
                        cid = JsonConvert.DeserializeObject<CarId>(msg.data.ToString());
                        if(driver.Car.Id.Equals(cid))
                        {
                            Console.WriteLine("TURBO started!");
                            driver.Turbo = null;
                            driver.TurboInUse = true;
                            driver.TurboStarted = true;
                        }
                        send(new Throttle(driver.Throttle, msg.gameTick));
                        break;
                    case "turboEnd":
                        cid = JsonConvert.DeserializeObject<CarId>(msg.data.ToString());
                        if(driver.Car.Id.Equals(cid))
                        {
                            Console.WriteLine("TURBO ended");
                            driver.TurboInUse = false;
                        }
                        send(new Throttle(driver.Throttle, msg.gameTick));
                        break;
					case "gameInit":
						Console.WriteLine("Race init");
                        obj = JToken.FromObject(msg.data);
                        Race race = obj["race"].ToObject<Race>();
                        if(driver.Race == null)
                        {
                            driver.Init(race);
                        }
                        else
                        {
                            driver.Init(race.Session);
                        }
                        send(new Throttle(driver.Throttle, msg.gameTick));
						break;
					case "gameEnd":
						Console.WriteLine("Race ended");
						send(new Ping());
						break;
                    case "crash":
                        obj = JToken.FromObject(msg.data);
                        cid = obj.ToObject<CarId>();
                        if (cid.Equals(driver.Id))
                        {
                            crashed = true;
                            driver.Crashed();
                        }
                        Console.WriteLine("Car " + obj["name"].ToString() + "(" + obj["color"].ToString() + ") crashed!");
                        send(new Throttle(driver.Throttle, msg.gameTick));
                        break;
                    case "spawn":
                        obj = JToken.FromObject(msg.data);
                        cid = obj.ToObject<CarId>();
                        if (cid.Equals(driver.Id))
                        {
                            crashed = false;
                        }
                        Console.WriteLine("Car " + obj["name"].ToString() + "(" + obj["color"].ToString() + ") spawned!");
                        send(new Throttle(driver.Throttle, msg.gameTick));
                        break;
					case "gameStart":
						Console.WriteLine("Race starts");
                        send(new Throttle(driver.Throttle, msg.gameTick));
						break;
					default:
                        Console.WriteLine("Additional JSON message: " + msg.msgType + " -> " + msg.data);
                        send(new Throttle(driver.Throttle, msg.gameTick));  
						break;
						}
	
					}
			catch(Exception e)
			{
				Console.WriteLine("Exception: " + e.ToString());
                send(new Ping());
			}
            TimeSpan span = DateTime.Now - time;
            if(span > new TimeSpan(0, 0, 0, 0, 50))
            {
                Console.WriteLine("***********************Handling the tick took: " + span.Seconds + "s " + span.Milliseconds + "mss");
            }
		}
	}

	private void send(SendMsg msg) {
		writer.WriteLine(msg.ToJson());
	}
}

class MsgWrapper {
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType, Object data) {
    	this.msgType = msgType;
    	this.data = data;
    }
}

class MsgWrapperWithTick : MsgWrapper 
{
    public int gameTick;

    public MsgWrapperWithTick(string msgType, Object data, int tick)
        : base(msgType, data)
    {
        this.gameTick = tick;
    }
}

abstract class SendMsg {
    public SendMsg()
    { 
    }
    public SendMsg(int tick)
    {
        gameTick = tick;
    }

	public string ToJson() {
        if (gameTick > 0)
        {
            return JsonConvert.SerializeObject(new MsgWrapperWithTick(this.MsgType(), this.MsgData(), gameTick));
        }
        else
        { 
            return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
        }
	}
	protected virtual Object MsgData() {
        return this;
    }

    protected abstract string MsgType();

    protected int gameTick = 0;
}

class Join: SendMsg {
	public string name;
	public string key;

	public Join(string name, string key) {
		this.name = name;
		this.key = key;
	}

	protected override string MsgType() { 
		return "join";
	}
}

class Ping: SendMsg {
    public Ping() { }
    public Ping(int tick) : base(tick) {}
	protected override string MsgType() {
		return "ping";
	}
}

class Throttle: SendMsg {
	public double value;

	public Throttle(double value, int tick) : base(tick){
		this.value = value;
	}

	protected override Object MsgData() {
		return this.value;
	}

	protected override string MsgType() {
		return "throttle";
	}
}

class SwitchLane : SendMsg
{
    public string value;

    public SwitchLane(string value, int tick) : base(tick)
    {
        this.value = value;
    }

    protected override Object MsgData()
    {
        return this.value;
    }

    protected override string MsgType()
    {
        return "switchLane";
    }
}

class UseTurbo : SendMsg
{
    public string value;

    public UseTurbo(string value)
    {
        this.value = value;
    }

    protected override Object MsgData()
    {
        return this.value;
    }

    protected override string MsgType()
    {
        return "turbo";
    }
}

class BotId
{
    public string name;
    public string key;
}

class CreateRace : SendMsg
{
    public BotId botId;
    public string trackName;
    public string password;
    public int carCount;

    public CreateRace(BotId id, string trackName, string password, int carCount)
    {
        this.botId = id;
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    protected override string MsgType()
    {
        return "createRace";
    }
}

class JoinRace : SendMsg
{
    public BotId botId;
    public string trackName;
    public string password;
    public int carCount;

    public JoinRace(BotId id, string trackName, string password, int carCount)
    {
        this.botId = id;
        this.trackName = trackName;
        this.password = password;
        this.carCount = carCount;
    }

    protected override string MsgType()
    {
        return "joinRace";
    }
}