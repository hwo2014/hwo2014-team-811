using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Datsuni
{
    public class Race
    {
        [JsonProperty(PropertyName = "track")]
        public Track Track { get; set; }

        [JsonProperty(PropertyName = "cars")]
        public List<Car> Cars { get; set; }

        [JsonProperty(PropertyName = "raceSession")]
        public RaceSession Session { get; set; }
    }

    public class Track
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "pieces")]
        public List<TrackPiece> Pieces { get; set; }

        [JsonProperty(PropertyName = "lanes")]
        public List<Lane> Lanes { get; set; }
    }

    public class TrackPiece
    {
        [JsonProperty(PropertyName = "length")]
        public double Length { get; set; }
        [JsonProperty(PropertyName = "switch")]
        public bool IsSwitch { get; set; }

        public bool IsStraight { get { return Length > 0.0; } }

        [JsonProperty(PropertyName = "radius")]
        public double Radius { get; set; }

        [JsonProperty(PropertyName = "angle")]
        public double Angle { get; set; }

        public List<double> MaxVelocitiesByLane = new List<double>();

        public List<double> TicksToNextSwitchByLane = new List<double>(); 
    }

    public class Lane
    {
        [JsonProperty(PropertyName = "distanceFromCenter")]
        public double DistanceFromCenter { get; set; }

        [JsonProperty(PropertyName = "index")]
        public int Index { get; set; }
    }

    public class CarId
    {
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "color")]
        public string Color { get; set; }

        public override bool Equals(Object o)
        {
            CarId id = o as CarId;
            if (id != null)
            {
                return Name == id.Name && Color == id.Color;
            }
            else
            {
                return false;
            }
        }


    }

    public class CarDimensions
    {
        [JsonProperty(PropertyName = "length")]
        public double Length { get; set; }

        [JsonProperty(PropertyName = "width")]
        public double Width { get; set; }

        [JsonProperty(PropertyName = "guideFlagPosition")]
        public double Position { get; set; }
    }

    public class TrackPiecePosition
    {
        [JsonProperty(PropertyName = "pieceIndex")]
        public int PieceIndex { get; set; }

        [JsonProperty(PropertyName = "inPieceDistance")]
        public double InPieceDistance { get; set; }

        [JsonProperty(PropertyName = "lane")]
        public LaneChange Lane { get; set; }

        [JsonProperty(PropertyName = "lap")]
        public int Lap { get; set; }
    }

    public class CarPosition
    {
        [JsonProperty(PropertyName = "id")]
        public CarId Id { get; set; }

        [JsonProperty(PropertyName = "angle")]
        public double Angle { get; set; }

        [JsonProperty(PropertyName = "piecePosition")]
        public TrackPiecePosition Position { get; set; }

        public double Velocity;
    }

    public class Car
    {
        [JsonProperty(PropertyName = "id")]
        public CarId Id { get; set; }

        [JsonProperty(PropertyName = "dimensions")]
        public CarDimensions Dimensions { get; set; }

        public bool GotBumpedInLap { get; set; }
        public bool BumpedInLap { get; set; }
    }

    public class RaceSession
    {
        [JsonProperty(PropertyName = "laps")]
        public int Laps { get; set; }

        [JsonProperty(PropertyName = "maxLapTimeMs")]
        public int MaxLapTime { get; set; }

        [JsonProperty(PropertyName = "quickRace")]
        public bool QuickRace { get; set; }
    }

    public class LaneChange
    {
        [JsonProperty(PropertyName = "startLaneIndex")]
        public int StartLaneIndex { get; set; }

        [JsonProperty(PropertyName = "endLaneIndex")]
        public int EndLaneIndex { get; set; }
    }

    public class Lap
    {
        [JsonProperty(PropertyName = "car")]
        public CarId Id { get; set; }

        [JsonProperty(PropertyName = "lapTime")]
        public LapTime LapTime { get; set; }

        [JsonProperty(PropertyName = "racetime")]
        public RaceTime RaceTime { get; set; }

        [JsonProperty(PropertyName = "ranking")]
        public Ranking Ranking { get; set; }
    }

    public class LapTime
    {
        [JsonProperty(PropertyName = "lap")]
        public int Lap { get; set; }

        [JsonProperty(PropertyName = "ticks")]
        public int Ticks { get; set; }

        [JsonProperty(PropertyName = "millis")]
        public int Millis { get; set; }
    }

    public class RaceTime
    {
        [JsonProperty(PropertyName = "laps")]
        public int Laps { get; set; }

        [JsonProperty(PropertyName = "ticks")]
        public int Ticks { get; set; }

        [JsonProperty(PropertyName = "millis")]
        public int Millis { get; set; }
    }

    public class Ranking
    {
        [JsonProperty(PropertyName = "overall")]
        public int Overall { get; set; }

        [JsonProperty(PropertyName = "fastestlap")]
        public int FastestLap { get; set; }
    }

    public class Turbo
    {
        [JsonProperty(PropertyName = "turboDurationTicks")]
        public int DurationTicks { get; set; }

        [JsonProperty(PropertyName = "turboFactor")]
        public double TurboFactor { get; set; }
    }
}